import Vuex from 'vuex'
const createStore = () => {
  return new Vuex.Store({
    state: {
      config: {
        marca: '',
        tdc: ''
      }
    }
  })
}

export default createStore
